@extends('layouts.app')

@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Edit Menu
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form" action="{{ action('Setting\MenuController@update', ['menu' => $menu->id]) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="kt-portlet__body">
            <div class="form-group">
                <label><b>Name</b> </label>&nbsp;<label class="text-danger">*</label>
                <input type="hidden" name="id" value="{{$menu->id}}" required>
                <input type="text" name="name" class="form-control" placeholder="Enter name" value="{{$menu->name}}" required>
                @if ($errors->has('name'))
                    <i class="text-danger">{{ $errors->first('name')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label><b>URL</b></label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="url" class="form-control" placeholder="Enter url" value="{{$menu->url}}" required>
                @if ($errors->has('url'))
                    <i class="text-danger">{{ $errors->first('url')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label><b>Icon</b></label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="icon" class="form-control" placeholder="Enter icon" value="{{$menu->icon}}" required>
                @if ($errors->has('icon'))
                    <i class="text-danger">{{ $errors->first('icon')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label for="parent"><b>Parent</b></label>&nbsp;<label class="text-danger">*</label>
                <select class="form-control kt-select2" name="parent" id="parent">
                    <option value=""></option>
                    <option value="0" selected>-- Root --</option>
                        @foreach($parents as $parent)
                            @if ($parent->id == $menu->parent)
                                <option value="{{ $parent->id  }}" selected>{{ $parent->name  }}</option>
                            @else
                                <option value="{{ $parent->id  }}">{{ $parent->name  }}</option>
                            @endif
                        @endforeach
                </select>
            </div>
            <label class="kt-checkbox">
                <input type="checkbox" name="show" {{ $menu->show === '1' ? 'checked' : '' }}> <b>Show at sidebar?</b>
                <span></span>
            </label>
            
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                <a href="{{ route('menu')}}" class="btn btn-secondary">Back</a>
            </div>
        </div>
        <br />
    </form>

    <!--end::Form-->
</div>

@endsection