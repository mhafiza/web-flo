@extends('layouts.app')

@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Tambah Menu
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form" action="{{ action('Setting\MenuController@store') }}" method="post">
        {{ csrf_field() }}
        <div class="kt-portlet__body">
            <div class="form-group">
                <label><b>Nama</b> </label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="name" class="form-control" placeholder="Masukkan nama menu" required>
                @if ($errors->has('name'))
                    <i class="text-danger">{{ $errors->first('name')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label><b>URL</b></label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="url" class="form-control" placeholder="Masukkan URL menu" required>
                @if ($errors->has('url'))
                    <i class="text-danger">{{ $errors->first('url')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label><b>Icon</b></label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="icon" class="form-control" placeholder="Masukkan icon menu" required>
                @if ($errors->has('icon'))
                    <i class="text-danger">{{ $errors->first('icon')  }}</i>
                @endif
            </div>
            <div class="form-group">
                <label for="parent"><b>Parent</b></label>&nbsp;<label class="text-danger">*</label>
                <select class="form-control kt-select2" name="parent" id="parent">
                    <option value=""></option>
                    <option value="0">Root</option>
                @foreach($parents as $key => $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
                </select>
            </div>
            <label class="kt-checkbox">
                <input type="checkbox" name="show"> <b>Tampilkan di sidebar?</b>
                <span></span>
            </label>
            
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                <a href="{{ route('menu')}}" class="btn btn-secondary">Back</a>
            </div>
        </div>
        <br />
    </form>

    <!--end::Form-->
</div>

@endsection