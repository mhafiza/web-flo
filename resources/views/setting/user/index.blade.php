@extends('layouts.app')

@section('content')

<!-- begin:: Content -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Users List
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="{{ route('users/create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="la la-plus"></i>
                        New Record
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="table_user">
            <thead>
                <tr>
                    <th>No</th>
                    <th>User Name</th>
                    <th>E-Mail</th>
                    <th>Role</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($user as $key => $value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{ isset($value->role->name) ? $value->role->name : '' }}</td>
                    <td>{{$value->created_at}}</td>
                    <td>{{$value->updated_at}}</td>
                    <td nowrap>
                        <a href="{{ route('users/edit', $value->id) }}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                          <i class="la la-edit"></i>
						</a>
						<form id="delete-{{$value->id}}"
                                action="#"
                                method="POST"
                                style="display: none;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                            onclick="remove({{$value->id}})">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <!--end: Datatable -->
    </div>
</div>


<!-- end:: Content -->
@endsection