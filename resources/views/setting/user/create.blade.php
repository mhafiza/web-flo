@extends('layouts.app')

@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Create User
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form">
        <div class="kt-portlet__body">
            <div class="form-group">
                <label>Name </label>&nbsp;<label class="text-danger">*</label>
                <input type="text" class="form-control" placeholder="Enter name" required>
            </div>
            <div class="form-group">
                <label>Email address</label>&nbsp;<label class="text-danger">*</label>
                <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                <span class="form-text text-muted">We'll never share your email with anyone else.</span>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>&nbsp;<label class="text-danger">*</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Re-Type Password</label>&nbsp;<label class="text-danger">*</label>
                <input type="match-password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="exampleSelect1">Role Select</label>&nbsp;<label class="text-danger">*</label>
                <select class="form-control" id="exampleSelect1">
                    <option>Super Admin</option>
                    <option>Admin</option>
                    <option>Operasional</option>
                </select>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="reset" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>

@endsection