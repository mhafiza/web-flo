@extends('layouts.app')

@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Tambah Role
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form" action="{{ action('Setting\RoleController@store') }}" method="post">
        {{ csrf_field() }}
        <div class="kt-portlet__body">
            <div class="form-group">
                <label><b>Name</b> </label>&nbsp;<label class="text-danger">*</label>
                <input type="text" name="name" class="form-control" placeholder="Masukkan nama role" value="{{ $role->name }}" required>
                @if ($errors->has('name'))
                    <i class="text-danger">{{ $errors->first('name')  }}</i>
                @endif
            </div>

            <div class="form-group">
                <label><b>Akses Menu</b> </label>&nbsp;<label class="text-danger">*</label>
                    <ul>
                        @php
                            function print_role_menu($menu, $is_child = FALSE){
                                $has_children = is_array($menu['children']) and isset($menu['children']);
                                if ($has_children) {
                                    echo "<li>";
                                    echo "<div class='checkbox'>";
                                    if ($menu->selected == true) {
                                        echo '<label><input checked type="checkbox" name="menus[]" value="'.$menu->id.'"/> '. $menu->name .'</label>';
                                    } else {
                                        echo '<label><input type="checkbox" name="menus[]" value="'.$menu->id.'"/> '. $menu->name .'</label>';
                                    }
                                    echo "</div>";
                                    echo "<ul>";
                                    foreach ($menu['children'] as $child){

                                        print_role_menu($child, TRUE);

                                    }
                                    echo "</ul>";
                                    echo "</li>";
                                } else {
                                    echo "<li>";
                                    echo "<div class='checkbox'>";
                                    if ($menu->selected == true) {
                                        echo '<label><input checked type="checkbox" name="menus[]" value="'.$menu->id.'"/> '. $menu->name .'</label>';
                                    } else {
                                        echo '<label><input type="checkbox" name="menus[]" value="'.$menu->id.'"/> '. $menu->name .'</label>';
                                    }
                                    echo "</div>";
                                    echo "</li>";

                                }
                            }
                        @endphp

                        @foreach($menus as $menu)
                            @php
                                print_role_menu($menu);
                            @endphp
                        @endforeach


                    </ul>
            </div>
        </div>


        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                <a href="{{ route('menu')}}" class="btn btn-secondary">Back</a>
            </div>
        </div>
        <br />
    </form>

    <!--end::Form-->
</div>

@endsection