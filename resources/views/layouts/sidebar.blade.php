@php


    function menus($role_id){
        $menus = App\Setting\Menu::all()->sortBy('id');

        $new_menus = [];
        $test = [];
        foreach ($menus as $menu){
            array_push($new_menus, $menu);
        }

        return build_tree($new_menus);
    }



    function build_tree($elements, $parentId = 0){
        $branch = array();

        foreach ($elements as $element) {

            if ($element['parent'] == $parentId) {
                $children = build_tree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    function print_menu($menu, $url, $is_child = FALSE){

        $has_children = is_array($menu['children']) and isset($menu['children']);
        if ($has_children) {
            if ((strpos(url('/').'/'.$menu['url'], $url) === 0) || (Request::segment(1) == $menu['url']) || (Request::segment(2) == $menu['url']) || (implode('/', [Request::segment(1), Request::segment(2)])  == $menu['url'])) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }

            $level_class = $menu['parent'] == 0 ? 'nav-second-level' : 'nav-third-level';
            if ($menu['parent'] == 0) {
                echo '<a href="#"><i class="'.$menu['icon'].'"></i> <span class="nav-label">'.$menu['name'].'</span> <span class="fa arrow"></span></a>';
            } else {
                echo '<a href="#"><span class="nav-label">'.$menu['name'].'</span> <span class="fa arrow"></span></a>';
            }

            if ((strpos(url('/').'/'.$menu['url'], $url) === 0) || (Request::segment(1) == $menu['url']) || (Request::segment(2) == $menu['url']) || (implode('/', [Request::segment(2), Request::segment(3)])  == $menu['url'])) {
                echo '<ul id="'.$menu['name'].'" data-current-url="'.$url.'" data-menu-url="'.$menu['url'].'" class="nav '.$level_class.' collapse in">';
            } else {
                echo '<ul id="'.$menu['name'].'" data-current-url="'.$url.'" data-menu-url="'.$menu['url'].'" class="nav '.$level_class.' collapse">';
            }


            foreach ($menu['children'] as $child){
                print_menu($child, url()->current(), TRUE);
            }

            echo '</ul>';
            echo '</li>';

        } else { // doesn't have children

            if ($is_child) {
                if ((Request::segment(1) == $menu['url']) || ( (implode('/', [Request::segment(1), Request::segment(2)])) == $menu['url']) || (implode('/', [Request::segment(1), Request::segment(2), Request::segment(3)])  == $menu['url']) ) {
                    echo '<li class="active"><a href='.url($menu['url']).'>'.$menu['name'].'</a></li>';
                } else {
                    echo '<li><a href='.url($menu['url']).'>'.$menu['name']  .'</a></li>';
                }
            } else {
                if ((Request::segment(1) == $menu['url']) || (implode('/', [Request::segment(1), Request::segment(2)])  == $menu['url']) || (implode('/', [Request::segment(2), Request::segment(3)])  == $menu['url']) ) {
                    echo "<li class='active'><a href='".url($menu['url'])."'><i class='".$menu['icon']."'></i><span class='nav-label'>".$menu['name']."</span></a></li>";
                } else {
                    echo "<li ><a href='".url($menu['url'])."'><i class='".$menu['icon']."'></i><span class='nav-label'>".$menu['name']."</span></a></li>";
                }
            }
        }
    }

    $menus = menus(\Illuminate\Support\Facades\Auth::user()->roles_id);

    foreach ($menus as $menu) {
        print_menu($menu, url()->current());
    }

@endphp