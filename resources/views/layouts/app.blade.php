<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>{{ isset($title) ? $title : config('app.name', 'Laravel')  }} | FLO Settlement BUJT</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Asap+Condensed:500">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{ asset('theme/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('theme/assets/css/pages/login/login-4.css') }} " rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{ asset('theme/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('theme/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{ asset('theme/assets/media/flo-logo.png') }}" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page-content-white kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

        @include('layouts.header')
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-container  kt-container--fluid  kt-grid kt-grid--ver">
						<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
							<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
							<!-- begin:: Aside -->
								<!-- begin:: Aside Menu -->
								<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
									<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
										<ul class="kt-menu__nav ">
											@include('layouts.menubar')
										</ul>
									</div>
								</div>
								<!-- end:: Aside Menu -->
							</div>
							<!-- end:: Aside -->
							<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

								<!-- begin:: Subheader -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-container  kt-container--fluid ">
										<div class="kt-subheader__main">
											<?php 
												$segment1 = Request::segment(1);
												$segment2 = Request::segment(2); 
											?>
											<span class="kt-subheader__separator kt-hidden"></span>
											<div class="kt-subheader__breadcrumbs">
												
												<a href="{{ url('/home') }}" class="kt-subheader__breadcrumbs-link">Home </a>
												
											@if (isset($title))
												@if (strtolower($segment1) != strtolower($title))
												@if (strtolower($segment1)
												== strtolower("configuration") || strtolower($segment1) == strtolower("process") )
												<span class="kt-subheader__breadcrumbs-separator"></span>
												<a href="{{ url($segment1) }}" class="kt-subheader__breadcrumbs-link"> {{ ucfirst($segment1) }} </a>
												@endif @endif

												@if (strtolower($segment2) != '' && strtolower($segment2) != strtolower($title) &&
													strtolower($segment1) !=
													strtolower("process") && strtolower($segment1) != strtolower("reject") && strtolower($segment1)
													!= strtolower("reporting") && strtolower($segment1) != strtolower("log") &&
													strtolower($segment2) !=
													strtolower("ipjaringan") )
												<span class="kt-subheader__breadcrumbs-separator"></span>
												<a href="{{ url($segment1.'/'.$segment2) }}" class="kt-subheader__breadcrumbs-link">
												{{ ucfirst($segment2) }} </a>
												@endif
											@endif
												<span class="kt-subheader__breadcrumbs-separator active"></span>
												<strong>{{ isset($title) ? $title : config('app.name', 'Laravel') }}</strong>
											</div>
										</div>
									</div>
								</div>

								<!-- end:: Subheader -->

								<!-- begin:: Content -->
								<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                    @yield('content')
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item" id="kt_footer">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-footer__wrapper">
								<div class="kt-footer__copyright">
									2020&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Keenthemes</a>
								</div>
								<div class="kt-footer__menu">
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">About</a>
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Team</a>
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Contact</a>
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{ asset('theme/assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('theme/assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="{{ asset('theme/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('theme/assets/plugins/custom/gmaps/gmaps.js') }}" type="text/javascript"></script>
		<script src="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="{{ asset('theme/assets/js/pages/dashboard.js') }}" type="text/javascript"></script>

		<!--end::Page Scripts -->


		@if (isset($js))
		<script src="{{ mix('js/' . $js) }}"></script>
		@endif

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>