@php


    function menus($role_id){
        $menus = App\Setting\Menu::all()->where('show',1)->sortBy('id');

        $new_menus = [];
        $test = [];
        foreach ($menus as $menu){
            array_push($new_menus, $menu);
        }

        return build_tree($new_menus);
    }



    function build_tree($elements, $parentId = 0){
        $branch = array();

        foreach ($elements as $element) {

            if ($element['parent'] == $parentId) {
                $children = build_tree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    function print_menu($menu, $url, $is_child = FALSE){
        $has_children = is_array($menu['children']) and isset($menu['children']);
        if ($has_children) {

            $level_class = $menu['parent'] == 0 ? 'nav-second-level' : 'nav-third-level';
            if ($menu['parent'] == 0) {
                echo "  <li class='kt-menu__section '>
                            <h4 class='kt-menu__section-text'>".$menu['name']."</h4>
                            <i class='kt-menu__section-icon flaticon-more-v2'></i>
                        </li>";
            }
            
            foreach ($menu['children'] as $child){
                print_menu($child, url()->current(), TRUE);
            }
            
        } else {
            if ((Request::segment(1) == $menu['url']) || (implode('/', [Request::segment(1), Request::segment(2)])  == $menu['url']) || (implode('/', [Request::segment(2), Request::segment(3)])  == $menu['url']) ) {
                echo "<li class='kt-menu__item  kt-menu__item--active' aria-haspopup='true'><a href='".url($menu['url'])."' class='kt-menu__link '><i class='kt-menu__link-icon  ".$menu['icon']."'></i><span class='kt-menu__link-text'>".$menu['name']."</span></a></li>";
            } else {
                echo "<li class='kt-menu__item  kt-menu__item' aria-haspopup='true'><a href='".url($menu['url'])."' class='kt-menu__link '><i class='kt-menu__link-icon ".$menu['icon']."'></i><span class='kt-menu__link-text'>".$menu['name']."</span></a></li>";
            }
        }
        
    }

    $menus = menus(\Illuminate\Support\Facades\Auth::user()->roles_id);

    foreach ($menus as $menu) {
        print_menu($menu, url()->current());
    }

@endphp

