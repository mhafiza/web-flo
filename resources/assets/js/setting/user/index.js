"use strict";
var KTDatatablesBasicPaginations = function() {

	var initTable1 = function() {
		var table = $('#table_user');

		// begin first table
		table.DataTable({
			responsive: true,
			pagingType: 'full_numbers',
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		}
	};
}();



jQuery(document).ready(function() {
	KTDatatablesBasicPaginations.init();
});

window.remove = function (id) {
	swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, delete it!',
		cancelButtonText: 'No, cancel!',
		reverseButtons: true
	}).then(function(result){
		if (result.value) {
			document.getElementById('delete-' + id).submit();
			swal.fire(
				'Deleted!',
				'Your file has been deleted.',
				'success'
			)
		} else if (result.dismiss === 'cancel') {
			swal.fire(
				'Cancelled',
				'Your record file is safe :)',
				'error'
			)
		}
	});
};
