# Web Flo Settlement Jasamarga - JMTO #

Project ini menggunakan framework Laravel 6.18

### Environment ###

* nodejs v6
* PHP 7.2.1

### Aristektur ###

* Web App menggunakan Laravel 6.18

### Mulai Development Project ###

* Gunakan command line / cmd / terminal
* Clone project dari repository
* cd web-flo/
* [npm](https://nodejs.org) install
* [composer](https://composer.org) install --prefer-dist
* php artisan clear:cache
* php artisan config:clear
* npm run watch
* buka tab terminal baru 
* php artisan serve