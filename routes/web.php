<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Setting')->group(function () {
    Route::prefix('setting')->group(function () {
        Route::group(['middleware' => 'auth'], function () {
            // User
            Route
            ::resource('users', 'UsersController');
            Route
            ::get('/users', 'UserController@index')
            ->name('users');
            Route
            ::get('/users/create', 'UserController@create')
            ->name('users/create');
            Route
            ::get('/users/{id}/edit', 'UserController@edit')
            ->name('users/edit');

            // Menu
            Route
            ::resource('menu', 'MenuController');
            Route
            ::get('/menu', 'MenuController@index')
            ->name('menu');
            Route
            ::get('/menu/create', 'MenuController@create')
            ->name('menu/create');
            Route
            ::get('/menu/{id}/edit', 'MenuController@edit')
            ->name('menu/edit');

            // Role
            Route
            ::resource('role', 'RoleController');
            Route
            ::get('/role', 'RoleController@index')
            ->name('role');
            Route
            ::get('/role/create', 'RoleController@create')
            ->name('role/create');
            Route
            ::get('/role/{id}/edit', 'RoleController@edit')
            ->name('role/edit');
        });
    });
});
