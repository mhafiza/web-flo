let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

// Setting -> Menu
mix.js(
   [
      'resources/assets/js/setting/menu/index.js'
   ],
   'public/js/setting/menu.js'
);

// Setting -> Role
mix.js(
   [
      'resources/assets/js/setting/role/index.js'
   ],
   'public/js/setting/role.js'
);

// Setting -> User
mix.js(
   [
      'resources/assets/js/setting/user/index.js'
   ],
   'public/js/setting/user.js'
);

