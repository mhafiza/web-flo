<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting\Role;
use App\Setting\Menu;
use App\Setting\RoleMenu;

class RoleController extends Controller
{
    private $js = 'setting/role.js';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();

        return view('setting/role/index')->with([
            'role' => $role,
            'title' => 'Role',
            'js' => $this->js
        ]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = $this->build_tree_role_menu(Menu::all());

        return view('setting/role/create')->with([
            'menus' => $menus,
            'title' => 'Tambah Role',
            'js' => $this->js
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role();
        $role->name = $request->input('name');

        $role->save();

        $menus = $request->input('menus');

        foreach ($menus as $menu) {
            $role_menu = new RoleMenu();
            $role_menu->roles_id = $role->id;
            $role_menu->menu_id = $menu;
            $role_menu->save();
        }

        return redirect('setting/role')->with('success', 'Berhasil Tambah Role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $menus = Menu::all();
        $selected_menus = RoleMenu::where('roles_id', $id)->get();

        foreach ($menus as $menu) {
            $selected = false;
            foreach ($selected_menus as $selected_menu) {
                if ($menu->id == $selected_menu->menu_id) $selected = true;
            }
            $menu->selected = $selected;
        }

        $menus = $this->build_tree_role_menu($menus);

        return view('setting/role/edit')->with([
            'role' => $role,
            'menus' => $menus,
            'selected_menus' => $selected_menus,
            'title' => 'Edit Role'
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function build_tree_role_menu($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {

            if ($element['parent'] == $parentId) {
                $children = $this->build_tree_role_menu($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}
