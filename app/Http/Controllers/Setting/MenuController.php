<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting\Menu;
use Validator;

class MenuController extends Controller
{
    private $js = 'setting/menu.js';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::all();
        $parents = Menu::menu_parent();

        return view('setting/menu/index')->with([
            'menu' => $menu,
            'parents' => $parents,
            'title' => 'Menu',
            'js' => $this->js
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parents = Menu::parents();   

        return view('setting/menu/create')->with([
            'parents' => $parents,
            'title' => 'Tambah Menu',
            'js' => $this->js
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu();
        $menu->name = $request->input('name');
        $menu->url = $request->input('url');
        $menu->icon = $request->input('icon');
        $menu->parent = $request->input('parent');
        $menu->show = $request->input('show') ? TRUE : FALSE;
        $menu->authorize_url = str_replace("/", "-", $request->input('url'));

        $menu->save();

        return redirect('setting/menu')->with('success', 'Berhasil Tambah Menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        $parents = Menu::parents();   

        return view('setting/menu/edit')->with([
            'parents' => $parents,
            'menu' => $menu,
            'title' => 'Edit Menu',
            'js' => $this->js
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $_menu = Menu::find($request->input('id'));

        $_menu->name = $request->input('name');
        $_menu->url = $request->input('url');
        $_menu->icon = $request->input('icon');
        $_menu->parent = $request->input('parent');
        $_menu->show = $request->input('show') ? TRUE : FALSE;
        $_menu->authorize_url = str_replace("/", "-", $request->input('url'));

        $_menu->save();

        return redirect('setting/menu')->with('success', 'Berhasil Update Menu '. $request->input('name'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);

        $menu->delete();

        return redirect('setting/menu')->with('success', 'Berhasil Hapus Menu');
    }
}
