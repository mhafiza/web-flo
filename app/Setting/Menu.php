<?php

namespace App\Setting;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = 'menu';

    public static function menu_parent()
    {
        $query = DB::table('menu')->select('parent')->get();

        $parent = [];

        foreach($query as $key => $list) {
            $nama = DB::table('menu')->select('name')->where('id', $list->parent)->first();
            
            if($nama == null) {
                $menu = "Root";
            } else {
                $menu = $nama->name;
            }
            array_push($parent, $menu);
        }
        return $parent;
    }

    public static function parents() {
        $query = DB::table('menu')->select('id','name')->where('parent', 0)->get();

        return $query;
    }
}
