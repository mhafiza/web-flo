<?php

namespace App\Setting;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
}
