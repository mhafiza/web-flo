<?php

namespace App\Setting;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
}
